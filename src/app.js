const linearScale = d3.scaleLinear()
  .domain([0, 100])
  .range([0, 600])
  .clamp(true)

console.log(linearScale(112))

const timeScale = d3.scaleTime()
  .domain([new Date(2018, 0, 1), new Date()])
  .range([0, 100])

console.log(timeScale.invert(50))

const quantizeScale = d3.scaleQuantize()
  .domain([0, 100])
  .range(["red", "white", "green"])

console.log(quantizeScale(22))
console.log(quantizeScale(50))
console.log(quantizeScale(88))

console.log(quantizeScale.invertExtent('white'))

const ordinalScale = d3.scaleOrdinal()
  .domain(['poor', 'good', 'great'])
  .range(['red', 'white', 'green'])

console.log(ordinalScale('good'))
console.log(ordinalScale('great'))
console.log(ordinalScale('poor'))